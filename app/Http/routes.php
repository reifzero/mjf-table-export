<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->get('/', function () {
    return <<<HTML
    <!DOCTYPE html>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
    <html>
      <head>
        <title>MJ Freeway Table Exporter</title>
      </head>
      <body>
        <div id="app">
        </div>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
HTML;
});


//started with with route prefix but controllers wouldn't resolve in the group
$app->get('api/tables', 'TableController@index');

$app->get('api/tables/{tableName}', 'TableController@show');

$app->post('api/tables', 'TableController@create');

$app->post('api/csv', 'CSVController@parseCSV');


$app->get('{catchall}', function () {
    return <<<HTML
    <!DOCTYPE html>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css">
    <html>
      <head>
        <title>MJ Freeway Table Exporter</title>
      </head>
      <body>
        <div id="app">
        </div>
        <script src="/static/bundle.js"></script>
      </body>
    </html>
HTML;
});
