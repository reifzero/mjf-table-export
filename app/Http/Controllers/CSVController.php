<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class CSVController extends Controller
{

  public function parseCSV(Request $request){
    if($request->has('csv')){
      $csv = $request->input('csv');
    }else{
      $csv = file_get_contents('php://input');
    }
    if($csv){
      foreach(str_getcsv($csv, PHP_EOL) as $row){
        $data[] = str_getcsv($row);
      }
      array_walk($data, function(&$a) use ($data) {
        $a = array_combine($data[0], $a);
      });
      array_shift($data);
      return $data;
    }else{
      abort(500);
    }
  }
}
