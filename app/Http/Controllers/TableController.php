<?php
namespace App\Http\Controllers;

use DB;
use Schema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;



class TableController extends Controller
{
    public function show($tableName)
    {

        return DB::table($tableName)->get();
    }

    public function index()
    {
      return DB::select(
          'select table_name from information_schema.tables where table_schema=?;',
          [env('DB_DATABASE')]
        );
    }

    public function create(Request $request){
      $data  = $this->cleanWhitespace($request->input('data'));
      $table_name = $request->input('table_name');
      $columnNames = array_keys($data[0]);
      Schema::dropIfExists($table_name);
      Schema::create($table_name, function(Blueprint $table) use ($columnNames){
        foreach($columnNames as $name){
          $table->string($name);
        }
      });
      foreach($data as $row){
        DB::table($table_name)->insert($row);
      }
      return DB::table($table_name)->get();
    }

    // started this, but don't really have time to differentiate on colummn types
    // protected function isColInt($col, $data){
    //   foreach($data as $row){
    //     if(!is_Integer($row[$col])){
    //       return false;
    //     }
    //   }
    //   return true;
    // }
    //
    // protected function isColTimeStamp($col, $data){
    //   foreach($data as $row){
    //     if(!isValidTimeStamp($row[$col])){
    //       return false;
    //     }
    //   }
    //   return true;
    // }

    protected function cleanWhitespace($data){
      $newData=[];
      foreach($data as $row){
        foreach ($row as $id => $val){
          $newRow[trim($id)] = $val;
        }
        array_push($newData,$newRow);
      }
      return $newData;
    }

}

?>
